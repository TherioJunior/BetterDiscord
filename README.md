# Discord

### Introduction

This is a repository focusing on the popular Discord enhancements [Vencord](https://vencord.dev) and [BetterDiscord](https://betterdiscord.app). In this, I will provide themes which are originally made for BetterDiscord, but are perfectly compatible with Vencord.

### Disclaimer

I am not affiliated with either of the two softwares.

Additionally, you should note that both of those go against the [Discord ToS](https://discord.com/terms).

### Downloads

- Download BetterDiscord [here](https://betterdiscord.app/).
- Download Vencord [here](https://vencord.dev/download/).

BetterDiscord's website detects your OS from your user agent I think and thus should deliver you a appropriate installer package automatically, while Vencord has buttons for selecting your OS, and includes installation commands and tutorials accordingly.

### Themes

For a list of themes I am using or rather switching through, please see the [Themes](./Themes) folder or look below to see the direct links to the BetterDiscord entries about them. These can be used with Vencord without additional configuration.

- [Cyberpunk 2077](https://betterdiscord.app/theme/Cyberpunk%202077)

- [ClearVision](https://betterdiscord.app/theme/ClearVision)

- [AMOLED Cord](https://betterdiscord.app/theme/AMOLED-Cord)

- [Discord+](https://betterdiscord.app/theme/Discord%2B)

- [Dark+](https://betterdiscord.app/theme/Dark%2B)

- [BlackHole](https://betterdiscord.app/theme/Black%20Hole)

### Plugins

Plugins update frequently, and my main Discord enhancement is Vencord, so instead of providing download links, I will simply mention which plugins I usually use by name.

- AlwaysTrust: Removes "untrusted domain" and "suspicious file" popups

- AnonymiseFileNames: Anonymise uploaded file names

- BetterFolders: Shows server folders on dedicated sidebar and adds folder related improvements

- BetterGifAltText: Change GIF alt text from simply being 'GIF' to containing the GIF tags / filename

- BetterUploadButton: Upload with a single click, open menu with right click

- CallTimer: Adds a timer to VCs

- ClearURLs: Removes tracking garbage from URLs

- Dearrow: Makes YouTube embed titles and thumbnails less sensationalist, powered by Dearrow

- EmoteCloner: Allows you to clone Emotes & Stickers to your own server (right click them)

- FakeNitro: Allows you to stream in nitro quality, send fake emojis/stickers and use client themes

- FavoriteGifSearch: Adds a search bar to favorite gifs

- FixImagesQuality: Fixes the quality of images in the chat being horrible

- ImageZoom: Lets you zoom in to images and gifs

- MessageClickActions: Hold Backspace and click to delete, double click to edit/reply

- MessageLogger: Temporarily logs deleted and edited messages

- NoScreensharePreview: Disables screenshare previews from being sent

- NoTypingAnimation: Disables the CPU-intensive typing dots animation

- PlatformIndicators: Adds platform indicators (Desktop, Mobile, Web...) to users

- ShowHiddenChannels: Show channels that you do not have access to view

- SilentTyping: Hide that you are typing

- VoiceChatDoubleClick: Join voice chats via double click instead of single click

- VolumeBooster: Allows you to set the user and stream volume above the default maximum
